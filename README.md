[![Published on webcomponents.org](https://img.shields.io/badge/webcomponents.org-published-green.svg)](https://www.webcomponents.org/element/arsnebula/nebula-dispatch)
[![Polymer Version](https://img.shields.io/badge/polymer-v2-blue.svg)](https://www.polymer-project.org)
[![Sauce Labs Build Status](https://img.shields.io/badge/saucelabs-passing-red.svg)](https://saucelabs.com/beta/builds/c5332849634f47c0ab51948a8326b79e)
[![Gitter Chat](https://badges.gitter.im/org.png)](https://gitter.im/arsnebula/webcomponents)
[![Become a Patreon](https://img.shields.io/badge/patreon-support_us-orange.svg)](https://www.patreon.com/arsnebula)

# \<nebula-dispatch\>

Send and receive action requests between web components.

* Can be used to implement a unidirectional data flow architecture
* Uses a standard custom `dispatch` event to facilitate communication between elements
* Listens for events on `window` so sender/receiver can be anywhere in document hierarchy
* Uses a `Promise` to allow action handler to resolve or reject the action and notify caller

## Installation

```sh
$ bower install -S arsnebula/nebula-dispatch
```

## Getting Started

Import the package.

```html
<link rel="import" href="/bower_components/nebula-dispatch/nebula-dispatch.html">
```

Use the `Nebula.DispatchBehavior` class extension mixin to add the `dispatchAction` method to any element. The `dispatchAction` method returns a `Promise` that allows the action handler to resolve or reject the action request.

```js
class MyElement extends Nebula.DispatchBehavior(Polymer.Element) {
  static get is() { return 'my-element' }
  ready() {
    super.ready()
    this.dispatchAction('MY_ACTION', {message: 'Hello World'}).then( (message) => {
      console.log('Action completed:', message)
    })
  }
}
```

Use the `Nebula.DispatcherBehavior` class extension mixin to automatically receive dispatch events, and invoke action handlers. Methods on the dispatcher element will be automatically invoked if the method name matches the action type.

```js
class MyActionsElement extends Nebula.DispatcherBehavior(Polymer.Element) {
  static get is() { return 'my-actions-element' }

  MY_ACTION(action) {
    try {
      // do stuff with payload
      const payload = action.payload

      action.resolve('Hello, nice to meet you')
    } catch(error) {
      action.reject(error)
    }
  }
}
```

*For more information, see the API documentation.*

## Contributing

We welcome and appreciate feedback from the community. Here are a few ways that you can show your appreciation for this package:

* Give us a **Star on GitHub** from either [webcomponents.org](https://www.webcomponents.org/element/arsnebula/nebula-element-mixin) or directly on [GitHub](https://github.com/arsnebula/nebula-element-mixin).

* Submit a feature request, or a defect report on the [Issues List](https://www.webcomponents.org/element/arsnebula/nebula-element-mixin/issues).

* Become a [Patreon](https://www.patreon.com/arsnebula). It takes a lot of time and effort to develop, document, test and support the elements in our [Nebula Essentials](https://www.webcomponents.org/collection/arsnebula/nebula-essentials) collection. Your financial contribution will help ensure that our entire collection continues to grow and improve.

If you are a developer, and are interested in making a code contribution, consider opening an issue first to describe the change, and discuss with the core repository maintainers. Once you are ready, prepare a pull request:

1. Fork it!
2. Create your feature branch: `git checkout -b my-new-feature`
3. Commit your changes: `git commit -am 'Add some feature'`
4. Push to the branch: `git push origin my-new-feature`
5. Submit a pull request :D

## Change Log

See [CHANGELOG](/CHANGELOG.md)

## License

See [LICENSE](/LICENSE.md)
