# Change Log

## [v2.0.2](https://github.com/arsnebula/nebula-dispatch/releases/tag/v2.0.2) (2017-05-01)

- Minor fixes to README documentation.

## [v2.0.1](https://github.com/arsnebula/nebula-dispatch/releases/tag/v2.0.1) (2017-05-01)

- Minor fixes to README documentation.

## [v2.0.0](https://github.com/arsnebula/nebula-dispatch/releases/tag/v2.0.0) (2017-04-20)

- Upgraded to Polymer v2 ES2015 class extension mixins. 

## [v1.0.0](https://github.com/arsnebula/nebula-dispatch/releases/tag/v1.0.0) (2017-02-26)

- Added initial release.